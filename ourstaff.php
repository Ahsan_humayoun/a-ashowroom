<!DOCTYPE html>
<html>
<head>
  <title>A&A showroom</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
html {
  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}
.main{
  width:100%;
  height:800px;
  margin-top: 120px;
}
.column {
  float: left;
  width: 33.3%;
  margin-bottom: 16px;
  padding: 0 8px;
}

@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}

.card {
  width:100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}

.container {
  padding: 0 16px;
}

.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title {
  color: grey;
}

.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}
</style>
</head>
<body>
  <?php 
include 'Controller/hdr_cont.php';
  ?>
<div class="main">
<center><h1>"Meet Our Team"</h1></center>
<br>

<div class="row">
  <div class="column">
    <div class="card">
      <img src="images/image1.png" alt="Jane" style="width:100%;"> <div class="container">
        <h2>Ahsan Humayoun</h2>
        <p class="title">CEO & Founder</p>
        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
        <p>example@example.com</p>
        <p><button class="button">Contact</button></p>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="images/image2.jpg" alt="Mike" style="width:100%">
      <div class="container">
        <h2>Assam Shah</h2>
        <p class="title">Director</p>
        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
        <p>example@example.com</p>
        <p><button class="button">Contact</button></p>
      </div>
    </div>
  </div>
  
  <div class="column">
    <div class="card">
      <img src="images/image3.jpg" alt="John" style="width:100%">
      <div class="container">
        <h2>Muhammad Ali</h2>
        <p class="title">incharge</p>
        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
        <p>example@example.com</p>
        <p><button class="button">Contact</button></p>
      </div>
    </div>
  </div>
</div>
</div>
<?php
include('footer.php');
?>
</body>
</html>
