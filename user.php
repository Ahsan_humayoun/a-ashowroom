<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="CSS/common.css" rel="stylesheet">
<link href="CSS/home.css" rel="stylesheet">

</head>
<body>
	<?php 
include 'Controller/hdr_cont.php';
	?>
<section>
	<div class="filter">
<div class="filter_search">
	<form>
		<p style="background-color:white;padding:20px; text-align: center;color:black; font-size:30px; margin-top: 10px; font-weight: bold;">Filter Search</p>
			<label>Model</label><select><br>
		<option>2000</option>
		<option>2001</option>
		<option>2002</option>
		<option>2003</option>
</select><br>
		<label>Make</label><select><br>
		<option>Honda</option>
		<option>Toyota</option>
		<option>Kia</option>
		<option>Hyundai</option>
</select><br>
	<label>location </label><select><br>
		<option>Islamabad</option>
		<option>Rawalpindi</option>
		<option>Karachi</option>
		<option>Lahore</option>
</select><br>
<label>Registration City </label>
<select>
		<option>Islamabad</option>
		<option>Rawalpindi</option>
		<option>Karachi</option>
		<option>Lahore</option>
</select><br>
<label>Price :</label><br>
<label>From </label>
<input type="range" min="10000" max="1000000" value="50" class="slider" id="slid1"><br>
<input type="number" value="10000"><br>
<label>To </label>
<input type="range" min="1000000" max="100000000" value="50" class="slider" id="slid2"><br>
<input type="number" value="1000000"><br>
<input type="button" name="fil_btn" value="Submit" style="height:40px;"></input>
</form>
</div>

	</div>
<div class="content">
<div class="gallery">
	<div class="search">
		<label>Sort By:</label><select>
		<option>Price(High to Low)</option>
		<option>Price(Low to High)</option>
		<option>Newest</option>
		<option>Oldest</option>
</select>
</div>
<div class="ad">
<a href="add.php"><img src="images/mehran1.jfif" alt="image"   /></a>
<div class="description">
	<a href="add.php"><label>Mehran 2002</label></a>
<p class="text"><b>City</b> : Rawalpindi</p>
<p class="text"><b>Model</b> : 2002</p>
<p class="text"><b>Price</b> : 350000</p>
<p class="text"><b>Posted On</b> : 4/24/2021</p>
</div>
</div>
<div class="ad">
<a  href="add.php"><img src="images/alto.jfif" alt="image" /></a>
<div class="description">
	<a href="add.php"><label>Alto for sale</label></a>
<p class="text"><b>City</b> : Islamabad</p>
<p class="text"><b>Model</b> : 2018</p>
<p class="text"><b>Price</b> : 525000</p>
<p class="text"><b>Posted On</b> : 4/23/2021</p>
</div>
</div>
<div class="ad">
<a  href="add.php"><img src="images/mehran.jfif" alt="image"/></a>
<div class="description">
<a href="add.php"><label>Mehran 2005</label></a>
<p class="text"><b>City</b> : Multan</p>
<p class="text"><b>Model</b> : 2005</p>
<p class="text"><b>Price</b> : 400000</p>
<p class="text"><b>Posted On</b> : 4/23/2021</p>
</div>
</div>
<div class="ad">
<a  href="add.php"><img src="images/liana.jfif" alt="image"/></a>
<div class="description">
<a href="add.php"><label>Liana</label></a>
<p class="text"><b>City</b> : Rawalpindi</p>
<p class="text"><b>Model</b> : 2007</p>
<p class="text"><b>Price</b> : 600000</p>
<p class="text"><b>Posted On</b> : 4/20/2021</p>
</div>
</div>
<div class="ad">
<a  href="add.php"><img src="images/sportage.jfif" alt="image"/></a>
<div class="description">
<a href="add.php"><label>Kia Sportage</label></a>
<p class="text"><b>City</b> : Rawalpindi</p>
<p class="text"><b>Model</b> : 2021</p>
<p class="text"><b>Price</b> : 4000000</p>
<p class="text"><b>Posted On</b> : 4/18/2021</p>
</div>
</div>
<div class="ad">
<a href="add.php"><img src="images/mehran2.jfif" alt="image" /></a>
<div class="description">
<a href="add.php"><label>Mehran for sale</label></a>
<p class="text"><b>City</b> : Islamabad</p>
<p class="text"><b>Model</b> : 2015</p>
<p class="text"><b>Price</b> : 750000</p>
<p class="text"><b>Posted On</b> : 4/15/2021</p>
</div>
</div>
<div class="ad">
<a href="add.php"><img src="images/toyota.jfif" alt="image" /></a>
<div class="description">
<a href="add.php"><label>Totyota Corolla 2007</label></a>
<p class="text"><b>City</b> : Islamabad</p>
<p class="text"><b>Price</b> : 1100000</p>
<p class="text"><b>Posted On</b> : 4/13/2021</p>
</div>
</div>
<div class="ad">
<a  href="add.php"><img src="images/alto.jfif" alt="image" /></a>
<div class="description">
	<a href="add.php"><label>Alto 2018</label></a>
<p class="text"><b>City</b> : Islamabad</p>
<p class="text"><b>Model</b> : 2018</p>
<p class="text"><b>Price</b> : 525000</p>
<p class="text"><b>Posted On</b> : 4/23/2021</p>
</div>
</div>
</div>
<div style="clear: both"></div>
	</section>
<?php 
include 'footer.php';
	?>
</body>
</html>