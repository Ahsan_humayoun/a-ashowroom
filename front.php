<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>A&A Homepage</title>

    <link rel="stylesheet" href="style.css">  
    <style type="text/css">
        
        * {
    margin: 0;
    padding: 0;
}
body {
     font-family: 'Brush Script MT', cursive
 }
.midpage {
    width: 1170px;
    margin: auto;
}
section {
    background: linear-gradient(rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.8)), url(https://cdn.luxe.digital/media/2020/12/15110747/fastest-cars-world-2021-luxe-digital%402x.jpg);
    height: 100vh;
    -webkit-background-size: cover;
    background-size: cover;
    background-position: center center;
    position: relative;
}
.nav-area {
    float: right;
    list-style: none;
    margin-top: 30px;
}
.nav-area li {
    display: inline-block;
}
.nav-area li a {
    color: #fff;
    text-decoration: none;
    padding: 5px 20px;
    font-family: poppins;
    font-size: 16px;
    text-transform: uppercase;
}
.nav-area li a:hover {
    background: #fff;
    color: #333;
}
.logo {
    float: left;
}
.welcome-text {
    position: absolute;
    width: 600px;
    height: 300px;
    margin: 20% 30%;
    text-align: center;
}
.welcome-text h1 {
    text-align: center;
    color: #fff;
    text-transform: uppercase;
    font-size: 60px;
}
.welcome-text h1 span {
    color: #00fecb;
}
.welcome-text a {
    border: 1px solid #fff;
    padding: 10px 25px;
    text-decoration: none;
    text-transform: uppercase;
    font-size: 14px;
    margin-top: 20px;
    display: inline-block;
    color: #fff;
}
.welcome-text a:hover {
    background: #fff;
    color: #333;
}

@media (max-width:600px) {
    .midpage {
        width: 100%;
    }
    .logo {
        float: none;
        width: 50%;
        text-align: center;
        margin: auto;
    }
   
    .nav-area {
        float: none;
        margin-top: 0;
    }
    .nav-area li a {
        padding: 5px;
        font-size: 11px;
    }
    .nav-area {
        text-align: center;
    }
    .welcome-text {
        width: 100%;
        height: auto;
        margin: 30% 0;
    }
    .welcome-text h1 {
        font-size: 30px;
    }
}

    </style>  
</head>
<body>
   <?php 
   session_start();
   $_SESSION['hdr']="true";
   $_SESSION['message']="";
include 'Controller/hdr_cont.php';
    ?>
    <section>
<div class="welcome-text">
        <h1>
A&A <span>ShowRoom</span></h1>
<a href="login.php">Login</a>
    </div>
</section>
<?php 
include 'footer.php';
    ?>
</body>
</html>
