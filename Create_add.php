<!DOCTYPE html>
<html>
<head>
	<title>Create Add</title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="CSS/common.css" rel="stylesheet">
<link href="CSS/Create_add.css" rel="stylesheet">
</head>
<body>
<?php 
include 'Controller/hdr_cont.php';
  ?>
	<section>
<div class="main">
  <form class="crt_ad" action="Controller/Craete_cont.php" method="post" enctype="multipart/form-data">
					    
					      <h1>Create Add</h1>
					      <p>Please fill in this form to create add.</p>
					      <hr><br>
                  <label for="id"><b>ID</b></label><br>
                <input type="text" placeholder="Enter ID" name="id" required>
<br><br>
 <label for="name"><b>Name</b></label><br>
                <input type="text" placeholder="Enter Name" name="name" required><br><br>
  <label for="city"><b>Registration City</b></label><br>
               <select name="city">
                <option>Rawalpindi</option>
                <option>Islamabad</option>
                <option>Multan</option>
                <option>Peshawar</option>
                <option>Karachi</option>
                <option>Quetta</option>
               </select><br><br>
                 <label for="model"><b>Model Year</b></label><br>
               <select name="model">
                <option>1990</option>
                <option>1991</option>
                <option>1992</option>
                <option>1993</option>
                <option>1994</option>
                <option>1995</option>
                 <option>1996</option>
                <option>1997</option>
                <option>1998</option>
                <option>1999</option>
                <option>2000</option>
                <option>2001</option>
                  <option>2002</option>
                <option>2003</option>
                <option>2004</option>
                <option>2005</option>
                <option>2006</option>
                <option>2007</option>
                 <option>2008</option>
                <option>2009</option>
                <option>2010</option>
                <option>2011</option>
                <option>2012</option>
                <option>2013</option>
                  <option>2014</option>
                <option>2015</option>
                <option>2016</option>
                <option>2017</option>
                <option>2018</option>
                <option>2019</option>
                <option>2020</option>
                <option>2021</option>
               
               </select><br><br>
               <label for="fuel"><b>Fuel Type</b></label><br>
               <select name="fuel">
                <option>Petrol</option>
                <option>CNG</option>
                  <option>Both</option>
               </select><br><br>
                <label for="gear"><b>Gear Box</b></label><br>
               <select name="gear">
                <option>Manual</option>
                <option>Automatic</option>
               </select><br><br>
                  <label for="engn"><b>Engine Type</b></label><br>
                <input type="number" placeholder="Enter Engine type" name="engn" required><br><br>
                  <label for="price"><b>Price</b></label><br>
                <input type="number" placeholder="Enter Price" name="price" required><br><br>
                 <label for="drv"><b>Kilometers Driven</b></label><br>
                 
                <input type="number" placeholder="Enter Kilometer's Driven" name="drv" required><br><br>
					   <label>Add Image</label><br><br>
  <input type="file" name="file1" id="fileToUpload1" hidden>
    <label for="fileToUpload1" class="lbl">Choose File</label><span id="file-chosen1" class="filename">No file chosen</span> <br><br>

    <input type="file" name="file2" id="fileToUpload2" hidden>
    <label for="fileToUpload2" class="lbl">Choose File</label><span id="file-chosen2" class="filename">No file chosen</span> <br><br>

     <input type="file" name="file3" id="fileToUpload3" hidden>
    <label for="fileToUpload3" class="lbl">Choose File</label><span id="file-chosen3" class="filename">No file chosen</span> <br><br>
  <label for="desc"><b>Description</b></label><br><br>
					 <textarea placeholder="Enter description" name="description" rows="10" cols="70"></textarea><br>

            
<a><button id="btn" type="submit" name="add">Create</button></a>			     
					    </div>
					  </form>



</div>
</section>
	<?php 
include 'footer.php';
	?>

</body>
<?php
include("javascript/add_data.js");
?>
</html>