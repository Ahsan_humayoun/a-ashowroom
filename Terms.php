<!DOCTYPE html>
<html>
<head>
	<title>
		Terms and Condition's
	</title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="CSS/common.css" rel="stylesheet">
	<link href="CSS/terms.css" rel="stylesheet">
</head>
<body>
		<?php 
include 'Controller/hdr_cont.php';
	?>
<section>
	<div class="outer">
	<h2 style="margin-bottom: 10px;">Terms Of Service</h2>
<div class="services">
<h3>Acknowledgment</h3>
<p>
If you wish to use this Website or employ PakWheels.com for any service, you must agree to the terms below as the exclusive basis which governs usage of the Website and provision of services. If you do not agree to any of the terms, do not use this Website or employ PakWheels.com for any service.

You acknowledge and agree that your usage (defined below) of the Website is strictly regulated and governed by the terms and conditions of this Agreement.
</p>
<h3>Introduction</h3>
<p>
Pak Wheels (Private) Limited (hereinafter referred to as “PakWheels” or “we” or “us” or “our”) welcome you at PakWheels for doing your car search, your search and access to all the products including car related products, car transaction service, forums, car inspection services, auto parts and accessories etc.

THESE WEBSITE TERMS OF USE ("TERMS OF USE") IS AN ELECTRONIC RECORD IN THE FORM OF AN ELECTRONIC CONTRACT FORMED UNDER THE LAWS OF PAKISTAN AND RULES MADE THEREUNDER AND THE AMENDED PROVISIONS PERTAINING TO ELECTRONIC DOCUMENTS. THESE TERMS OF USE DOES NOT REQUIRE ANY PHYSICAL, ELECTRONIC OR DIGITAL SIGNATURE.

The domain name www.PakWheels.com is owned by PakWheels (Private) Limited, a company duly incorporated under the Laws of Pakistan with its registered office at 2nd Floor, Saeed Alam Tower, 37-Commercial Zone, Liberty Market, Gulberg, Lahore. These terms of use of the Website available at www.PakWheels.com, mobile site and mobile applications (individually and collectively, “Website”) is between PakWheels and the guest users or registered users of the Website (hereinafter referred to as "you" or "your" or "yourself" or "user" or “customer”) describe the terms on which PakWheels.com offers you access to the Website for availing services and buy products, as are incidental and ancillary thereto ("Services", and more particularly defined hereinafter).

These terms of service are effective from 1st January, 2021
</p>
<h3>Eligibility to use services</h3>
<p>
Service available through or use of the Website is available only to persons of majority (18 years of age or more) and who can legally component to form a contract under the  Contract Act, 1872 (as amended from time to time), or any other act or enactments to which the person is subject.

Persons who are incompetent, to make a legal and valid contract, for any reason whatsoever (minor, lunatic, insane, undischarged insolvent or otherwise) are not eligible to use our Website and avail our services. BY ACCESSING OUR WEBSITE OR AVAILING ANY OF OUR SERVICES, YOU REPRESENT TO THE PAKWHEELS.COM THAT YOU ARE MAJOR AND LEGALLY COMPETENT TO FORM A VALID CONTRACT UNDER THE TERMS OF USE.

Further to this, PakWheels reserves its right, without assigning any reason whatsoever, to restrict or limit your access to the Website and can further terminate your access to Website and deny the services or any other product available through or at our Website. This right of suspension / termination of services of PakWheels, is in addition to any other remedy available to PakWheels, for access & usage of Website or availing any of our services through Website, which is in contravention of any of the terms and conditions of terms of use or this agreement or any other applicable law.
</p>

<h3>Important</h3>
<p>
This Website, including mobile application, is an online marketplace and which provides a platform to users to avail the services. Any information provided either through or at Website is only for information purpose and such information does not substitute any specific advice whether investment, legal, taxation or otherwise and are not intended to provide you with any nature of certification, guarantee or warranty. PakWheels is not responsible and cannot be held liable for any transaction between the Website’s users.

By accessing, browsing and using this Website or availing services, you agree and acknowledge that, you understand this limited and restricted use and accessing, browsing or using this Website or availing any services is out of your own free will, discretion and responsibility.

You expressly agree that any information provided on the Website shall be used by you at your risk. You are advised to hereby make proper enquiries and use the information provided on the Website.

From time to time PakWheels may supplement its terms of use with additional terms pertaining to specific content and your usage of mobile app may further require you to abide by the additional terms of third party e.g. play store etc. before download and installing the mobile app. These terms are collectively referred to as (“additional terms”). Such additional terms are hereby incorporated by reference into these terms of Use.
</p>
<h3>
Right of terminate/suspend/discontinue of any service</h3>
<p>
PakWheels reserves its right to modify, suspend, cancel or discontinue any or all sections, or services at any time without any notice. PakWheels reserves the right to make modifications and alterations in the information contained on the Website without notice. You agree that PakWheels shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Website.

PakWheels reserves the right to terminate any service agreement without any notice to the user and without providing any reason whatsoever. In the event of cancellation, the user shall pay any outstanding invoices relating to the service agreement within the next 10 working days.
</p>
<h3>
User’s obligation and restrictions</h3>
<p>
Subject to the compliance with all applicable laws, rules and terms and conditions of this agreement, you acknowledge, agree and undertake that your use of the Website shall be strictly governed by this agreement (including term of use) and the following binding principles.

You shall not use or access the Website or avail the services by any means other than through the interface that are provided by PakWheels when you use the Website or Website and/or the services you specifically undertake not to host, display, upload, modify, publish, transmit, update or share any information or content or user content that;
	</p>
</div>
</div>
</section>
<?php 
include 'footer.php';
	?>

</body>
</html>