<!DOCTYPE html>
<html>
<head>
	<title>Add</title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="CSS/common.css" rel="stylesheet">
<link href="CSS/adds.css" rel="stylesheet">

</head>
<body>
	<?php 
	session_start();
include 'Controller/hdr_cont.php';
	?>
<section>
	<?php
if(isset($_POST["ibtn"])){
$hostname="localhost";
$username="root";
$password="";
$databaseName="a&ashowroom";
$id=$_POST["adid"];
$connect=mysqli_connect($hostname,$username,$password,$databaseName);
$query="SELECT * FROM `create_add`";
$result=mysqli_query($connect,$query);
while($row=mysqli_fetch_array($result)){
	if($row["id"]==$id){
		$image1=$row["image1"];
		$image2=$row["image2"];
		$image3=$row["image3"];
	$name=$row["name"];
$city=$row["reg_city"];
$model=$row["model_year"];
$price=$row["price"];
$fuel=$row["fuel_type"];
$tranmission=$row["transmission"];
$eng=$row["engine_cap"];
$driven=$row["driven_km"];
$desc=$row["description"];
echo "<div class='add' style='height:1000px;'>
	<div id='main'>
<div class='inner fade'>
<a target='_blank' href='uploads/$image1'><img src='uploads/$image1' alt='image'/></a>
</div>
<div class='inner fade'>
<a target='_blank' href='uploads/$image2'><img src='uploads/$image2' alt='image' /></a>
</div>
<div class='inner fade'>
<a target='_blank' href='uploads/$image3'><img src='uploads/$image3' alt='image'/></a>
</div>
<a class='next' onclick='plusslide(-1)'>></a>
<a class='prev' onclick='plusslide(1)'><</a>
</div>
<div class='desc'>
<label>$name</label></a>
<p class='text'><b>City</b> : $city</p>
<p class='text'><b>Model</b> : $model</p>
<p class='text'><b>Price</b> : $price</p>
<p class='text'><b>Posted On</b> : 4/24/2021</p>
</br>
<div class='cmnt'>
	<h2 style='margin-top:10px;''>Seller Comment</h2>
<p   style='margin-bottom: 10px;'>$desc</p>
	</div>
</div>
</div>
<div class='sell_info' style='height: 300px;''>
<center><label>Contact Seller</label></center>
<p><b>Name: </b>Ahsan Humayoun Saleem</p>
<p><b>Email: </b>Ahkhan1o71@gmail.com</p>
<p><b>Address: </b>Rawalpindi Cannt</p>
<p><b>Contact: </b>+92316528997</p>
	</div>
<div class='sell_info' style='height: 300px;'>
<center><label>Features</label></center><br>
<div style='margin-top: 40px;''>
<div style='margin-left: 30px; display: inline-block;'>
<img src='images/meter.png' alt='Image' style='width:70px;height:70px;'/>
<h3>Mileage</h3>
<h4>$driven</h4>
</div>
<div  style='margin-left: 70px; display: inline-block;''>
<img src='images/fuel.png' alt='Image' style='width:70px;height:70px;'/>
<h3>Fuel</h3>
<h4>$fuel</h4>
</div>
<div  style='margin-left: 70px; display: inline-block;''>
<img src='images/engine.png' alt='Image' style='width:70px;height:70px;'/>
<h3>Engine</h3>
<h4>$eng</h4>
</div>
<div  style='margin-left: 70px; display: inline-block;'>
<img src='images/gear.jpg' alt='Image' style='width:70px;height:70px;'/>
<h3>Shifter</h3>
<h4>$tranmission</h4>
</div>
</div><br><br><br><br>
";  
	}
}
}
?>
<div class='sell_info' style='height: 315px;width: 580px;margin-right: 0px;'>
<center><label>Inspection Service</label></center>
<p>Want to get a detail report on vehicle condition</p>
<p>Get Our Inspection Team Services</p>
<a class='log' href='Services.php' style='float:right;'>
	<input type='button' value='Get Now' style='width:160px;height:50px; position:relative; top:60px;right:200px;background-color: blue;font-size: 20px;'/></a>
	</div>
<br>
	</div>
	<div style="clear: both;">
	</div>
</section>
<?php 
include 'footer.php';
	?>
</body>
<script type="text/javascript">
 var slideindex = 1;
 showslides(slideindex);
function plusslide(n){
  showslides(slideindex += n);
}
function showslides(n){
  var i;
 var slides=document.getElementsByClassName("inner");
if(slideindex>slides.length)
  slideindex=1;
if(slideindex<1)
  slideindex=slides.length;
for(i=0;i<slides.length;i++){
   slides[i].style.display="none";
}
slides[slideindex-1].style.display="block";
}
  </script>
</body>
