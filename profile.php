<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="CSS/common.css" rel="stylesheet">
	<link href="CSS/profile.css" rel="stylesheet">
</head>
<body>
			<?php 
	session_start();
include 'Controller/hdr_cont.php';
	?>
<section>
	<center><h2>Profile</h2></center>
<form action="Controller/prof_cont.php" method="post">
	  <label for="id"><b>ID</b></label>
                <input type="text" name="id" value="<?php  echo $_SESSION['id']; ?>" disabled><br>
 <label for="name"><b>Name</b></label>
                <input type="text"  name="name" value="<?php  echo $_SESSION['name']; ?>" required><br>
					      <label for="email"><b>Email</b></label>
					      <input type="email"  name="email" value="<?php  echo $_SESSION['email']; ?>"  required><br>

					      <label for="psw"><b>Password</b></label>
					      <input type="password"  name="psw" value="<?php  echo $_SESSION['password']; ?>"  required><br>
					      <label for="psw-repeat"><b>Repeat Password</b></label>
					      <input type="password" placeholder="Repeat Password" name="psw-repeat" required><br>

                <label for="cnt"><b>Contact</b></label>
                <input type="number" name="contact" value="<?php  echo $_SESSION['ph_number']; ?>"  required><br>

  <label for="city"><b>City</b></label>
               <select name="city" value="<?php  echo $_SESSION['city']; ?>" >
                <option>Rawalpindi</option>
                <option>Islamabad</option>
                <option>Multan</option>
                <option>Peshawar</option>
                <option>Karachi</option>
                <option>Quetta</option>
               </select><br>
 <label for="addr"><b>Address</b></label>
                <input type="text"  name="address" value="<?php  echo $_SESSION['address']; ?>"  required><br>
					  
<a><button class="btn" type="submit" name="update">Update</button></a><br><br>
<a><button class="btn" type="submit" name="delete">Delete Account</button></a>
</form>
<div style="float:right;clear:left;">
<a href="Controller/pdf.php"><button class="dld" style="width:100px;height:50px; background-color:#5CAF50; color:white; cursor:pointer;">Dowload PDF</button></a>
	<a href="Controller/xls.php"><button class="dld" style=" width:100px;height:50px; background-color:#5CAF50; color:white; cursor:pointer;">Dowload Excel</button></a>

</div>
	<br><br><br>
</section>

	<?php 
include 'footer.php';
	?>
</body>
<script type="text/javascript">
//document.getElementById('id').value= ;
	</script>
</html>