 
<?php
 session_start();
   ?>
<!DOCTYPE html>
<html>
<head>
	<title>Login A&A showroom </title>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="CSS/common.css" rel="stylesheet">
<link href="CSS/login.css" rel="stylesheet">
<style type=text/css>
#body{width: 1227px; height: auto; margin: 0 auto;}

    #head
    {
         background-color: #fff602;
         width: 755px;
         height: 50px;
         padding: 100px 0 0 20px;
         font-family: lato;
         font-size: 30px;
text-align: center;
    }
    form
    {
         background-color: #ffffff;
         width: 700px;
         height: 500px;
         padding: 100px 0 0 75px;
         font-family: lato;
         font-size: 20px;
    }
    input.inp
    {
        width: 525px;
         height: 40px;
         font-size: 20px;
         padding: 0px 0 0px 10px; 
         border-color: #ababa9;
         border-width: 1px;
         margin-top: 10px;
    }
    p
    {
        font-family: lato;
        color: #ababa9;
        font-size: 16px;
    }
    #btn
    {
        background-color: #fff602;
        height: 40px;
    width: 150px;
        margin: 100px 0 40px 20px;
        border:none;

    }
    #btn:hover{
    cursor: pointer;
    color:red;
    }
    label{
        font-size: 20px;
        font-family: verdana;
    }


</style>
</head>
<body>
	<?php 
include 'Controller/hdr_cont.php';
    ?>
<div id="body">
	<div id="head">
	<strong>LOG IN</strong>
	</div>
	<form action="Controller/log_cont.php" method="post">
		<label>Username</label><br>
	<input class="inp" type="text" name="id" placeholder="Enter Username" required><br><br>
<label>Password</label><br>
	<input class="inp" type="password" name="password" placeholder="Enter password" required>

	<p name="message" style="color:red;"><?php  echo  $_SESSION['message']; ?> </p>
<a><button id="btn" type="submit" name="log">Log In</button></a>

<p style="">Not a User?</p>
<a href="sign.php"><p>Register now</p></a>
	
</form>
</div>
<?php 
include 'footer.php';
	?>
</body>
</html>
<?php
$_SESSION['message']="";
?>